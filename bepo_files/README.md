# Installation Linux :

Copier le contenu de layout.klc dans /usr/share/X11/xkb/symbols/fr  
Activation manuelle :
```sh
$ setxkbmap fr bepo_antoine
```
Revenir à azerty :
```sh
$ setxkbmap fr occ
```

# Installation Windows :

Éxécuter bepo_files/antoine0/setup.exe


# Procédure complète de création des drivers :

Installer [pilotes](https://gitlab.com/bepo/pilotes) avec l'aide de <http://bepo.fr/wiki/ConfigGenerator>.  
Remplacer pilotes/configGenerator/layout.conf par ./layout.conf puis :
```sh
$ ./pilotes/configGenerator/runAll.sh
```

## Linux

Source <http://bepo.fr/wiki/Utilisateur:Arathor/B%C3%A9po-altgrsym-3>  
Apporter les modifications suivantes dans pilotes/configGenerator/results/layout.xkb :
```
xkb_symbols "bepo_antoine" {                        // MODIFIED bepo_antoine

name[Group1]= "Bépo antoine";                       // MODIFIED "Bépo antoine"

    // Modificateurs                                // MODIFIED LINE
    // AltGr symmétrique                            // MODIFIED LINE
    include "level3(ralt_switch)"                   // MODIFIED LINE
    include "level3(lalt_switch)"                   // MODIFIED LINE
    // [²] devient Alt                              // MODIFIED LINE
    key <TLDE> { [ Alt_L, Alt_L, Alt_L, Alt_L ] };  // MODIFIED LINE

key <TLDE> { type[group1] = "FOUR_LEVEL", [ heart, heart, heart, heart ] }; // À SUPPRIMER
```

layout.xkb est prêt.

## Windows

Aide : <http://bepo.fr/wiki/Pilote_Windows> et <http://bepo.fr/wiki/Utilisateur:LeBret/Remplacer_AltGr_par_Kana>  

Copier ./bepoA-kbd.klc dans C:\Program Files (x86)\Microsoft Keyboard Layout Creator 1.4\bin\i386  
Effectuer les modifications suivantes :
```
// KBD	bepoxxxx	"Bépo vxxx"                    // DELETED LINE
KBD	antoine0	"Bepo_antoine v0"              // ADDED   LINE

COPYRIGHT	"CC-SA-BY"

// COMPANY	"Disposition bépo - https://bepo.fr"   // DELETED LINE
COMPANY	"Disposition bépo adaptée par antoine"         // ADDED   LINE

LOCALENAME	"fr-FR"

LOCALEID	"0000040c"

// VERSION	xxxx                                   // DELETED LINE
VERSION	0                                              // ADDED   LINE

// À la fin

// 0409	French (bépo vxxxx)                    // DELETED LINE
0409	French (bepo_antoine v0)               // ADDED   LINE
// 040C	Français (bépo vxxxx)                  // DELETED LINE
040C	Français (bepo_antoine v0)             // ADDED   LINE
```

Lancer MSKLC qui est dans C:\Program Files (x86)\Microsoft Keyboard Layout Creator 1.4  
Générer l’installateur de la disposition.

Aller dans C:\Program Files (x86)\Microsoft Keyboard Layout Creator 1.4\bin\i386 avec  Command ou PowerShell en mode administrateur pour générer les sources C :
```pwsh
> cd "C:\Program Files (x86)\Microsoft Keyboard Layout Creator 1.4\bin\i386"
> kbdutool.exe -u -s bepoA-kbd.klc
```

ajouter à la fin de antoine0.H :

```
#undef T38
 #define T38 _EQ(                                       KANA                      )
#undef X38
 #define X38 _EQ(                                       KANA                      )
#undef T29
 #define T29 _EQ(                                      LMENU                      )
```

modifier dans antoine0.C :

```
static ALLOC_SECTION_LDATA VK_TO_BIT aVkToBits[] = {
    { VK_SHIFT    ,   KBDSHIFT     },
    { VK_CONTROL  ,   KBDCTRL      },
    { VK_MENU     ,   KBDALT       },
    { VK_KANA     ,   KBDKANA      },                 // ADDED   LINE
    { 0           ,   0           }
};


static ALLOC_SECTION_LDATA MODIFIERS CharModifiers = {
    &aVkToBits[0],
    // 7,                                             // DELETED LINE
    9,                                                // ADDED   LINE
    {
    //  Modification# //  Keys Pressed
    //  ============= // =============
        0,            //
        1,            // Shift
        2,            // Control
        SHFT_INVALID, // Shift + Control
        SHFT_INVALID, // Menu
        SHFT_INVALID, // Shift + Menu
        // 3,            // Control + Menu            // DELETED LINE
        // 4             // Shift + Control + Menu    // DELETED LINE
        SHFT_INVALID, // Control + Menu               // ADDED   LINE
        SHFT_INVALID, // Shift + Control + Menu       // ADDED   LINE
        3,            // kana                         // ADDED   LINE
        4             // kana + shift                 // ADDED   LINE
     }
};

  // {'R'          ,CAPLOK | CAPLOKALTGR,'o'      ,'O'      ,WCH_NONE ,'$'      ,0x2665   },  // DELETED LINE
  {'R'          ,CAPLOK ,'o'      ,'O'      ,WCH_NONE ,'$'      ,0x2665   },                  // ADDED LINE


    // MAKELONG(KLLF_ALTGR, KBD_VERSION), // DELETED LINE
    MAKELONG(0, KBD_VERSION),             // ADDED   LINE
```

Générer les DLLs :
```pwsh
> attrib +R antoine0.H
> attrib +R antoine0.C
> attrib +R antoine0.RC
> attrib +R antoine0.DEF
>
> kbdutool.exe -u -x bepoA-kbd.klc
> kbdutool.exe -u -i bepoA-kbd.klc
> kbdutool.exe -u -o bepoA-kbd.klc
> kbdutool.exe -u -m bepoA-kbd.klc
```
Copier à chaque fois le DLL dans le dossier correspondant de l’installateur  

L’installeur est prêt.
