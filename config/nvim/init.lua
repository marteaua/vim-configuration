-- #---------------#
-- | neovim config |
-- #---------------#

require 'utils'

require 'settings'

require 'keymappings'

require 'plugins'

require 'colorscheme'

require 'lsp'
