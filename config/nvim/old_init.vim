set noerrorbells
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set nu
set relativenumber
set nowrap
set smartcase
set nobackup
set incsearch
set clipboard+=unnamedplus
set splitbelow              " A new window is put below the current one
set splitright              " A new window is put right of the current one
" coc plugin related
set hidden
set nobackup
set nowritebackup
set cmdheight=2
set scrolloff=8
set updatetime=300
set shortmess+=c
set listchars=nbsp:·
set list

set colorcolumn=80

"Coloration syntaxique des .pro comme pour .cpp
autocmd BufNewFile,BufReadPost *.pro set filetype=cpp
autocmd BufNewFile,BufReadPost *.geo set filetype=cpp

"Pugins
call plug#begin('~/.config/nvim/plugged')

Plug 'vim-airline/vim-airline' " status bar (needs special fonts)
Plug 'ryanoasis/vim-devicons' " various symbols (linux, rust, python, ...)
Plug 'gruvbox-community/gruvbox' " color scheme

Plug 'junegunn/fzf', {'do': { -> fzf#install() } } " autocompletion recherches
Plug 'junegunn/fzf.vim'


Plug 'SirVer/ultisnips' " snipet engine

Plug 'tpope/vim-fugitive' "git integrations
Plug 'stsewd/fzf-checkout.vim' "gestion branches et tags avec fzf

Plug 'neoclide/coc.nvim', {'branch': 'release'} " completion

" formatage code
Plug 'chiel92/vim-autoformat'

" c++/C config
Plug 'jackguo380/vim-lsp-cxx-highlight' "c++ highlighting

" explore source file structure
Plug 'liuchengxu/vista.vim'

" debugger
Plug 'puremourning/vimspector'

Plug 'ThePrimeagen/vim-be-good'  " vim tutorial game by The Primeagen

" Latex
Plug 'lervag/vimtex'

" Julia
Plug 'JuliaEditorSupport/julia-vim'

call plug#end()

highlight ColorColumn ctermbg=0 guibg=lightgrey

highlight Normal guibg=NONE ctermbg=NONE

"desactive les flèches directionelles
noremap <Up> <nop>
inoremap <Up> <nop>
noremap <Down> <nop>
inoremap <Down> <nop>
noremap <Right> <nop>
inoremap <Right> <nop>
noremap <Left> <nop>
inoremap <Left> <nop>

"Normal mode dans terminal
tnoremap <A-q> <C-\><C-n>

"LeaderKey
let mapleader = " "
if empty($BEPO) "azerty
    let maplocalleader = "ù"
else
    let maplocalleader = "ç"
endif

" Rafraichir les racourcis
nnoremap <leader>ç :so /home/antoine/dotfiles/config/nvim/init.vim<cr>

" Bindings dépendants de la disposition (azerty ou bépo)
if empty($BEPO) "azerty

    " Deplacement entre splits
    nnoremap <leader>h :wincmd h<CR>
    nnoremap <leader>j :wincmd j<CR>
    nnoremap <leader>k :wincmd k<CR>
    nnoremap <leader>l :wincmd l<CR>

else    " bépo

    noremap à :
    noremap é :!<space>

    " [HJKL] -> {CTSR}
    noremap c h
    noremap r l
    noremap t j
    noremap s k
    noremap C H
    noremap R L
    noremap T J
    noremap S K
    noremap zs zj
    noremap zt zk
    " {HJKL} <- [CTSR]
    " {J} = « Jusqu'à »            (j = suivant, J = précédant)
    noremap j t
    noremap J T
    " {L} = « Change »             (l = attend un mvt, L = jusqu'à la fin de ligne)
    noremap l c
    noremap L C
    " {H} = « Remplace »           (h = un caractère slt, H = reste en « Remplace »)
    noremap h r
    noremap H R
    " {K} = « Substitue »          (k = caractère, K = ligne)
    noremap k s
    noremap K S
    " Corollaire : correction orthographique
    noremap ]k ]s
    noremap [k [s
    " ligne écran précédente / suivante (à l'intérieur d'une phrase)
    noremap gs gk
    noremap gt gj
    " onglet précédent / suivant
    noremap gb gT
    noremap gé gt
    " optionnel : {gB} / {gÉ} pour aller au premier / dernier onglet
    noremap gB :exe "silent! tabfirst"<CR>
    noremap gÉ :exe "silent! tablast"<CR>
    " optionnel : {g"} pour aller au début de la ligne écran
    noremap g" g0

    " Pour faciliter les manipulations de fenêtres, on utilise {ê} comme un Ctrl+W :
    noremap ê <C-w>
    noremap Ê <C-w><C-w>
    noremap êt <C-w>j
    noremap ês <C-w>k
    noremap êc <C-w>h
    noremap êr <C-w>l
    noremap êd <C-w>c
    noremap êo <C-w>s
    noremap êp <C-w>o
    noremap ê<SPACE> :vsplit<CR>
    noremap ê<CR> :split<CR>

    " fix les deplaement haut bas et le tri des entrées dans Ex (nav. fichier vim)
    augroup netrw_dvorak_fix
        autocmd!
        autocmd filetype netrw call Fix_netrw_maps_for_dvorak()
    augroup END
    function! Fix_netrw_maps_for_dvorak()
        noremap <buffer> t j
        noremap <buffer> s k
        noremap <buffer> k s
    endfunction

    " Affiche les espaces insécables
    set listchars=nbsp:·
    set list

endif " azerty / bepo

" Highlight trailing whitespace
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
au BufWinEnter * match ExtraWhitespace /\s\+$/
au InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
au InsertLeave * match ExtraWhitespace /\s\+$/
au BufWinLeave * call clearmatches()

" Remove trailing whitespace on save
autocmd BufWrite * %s/\s\+$//e

" Acces a doc c++ cppman
function! s:JbzCppMan()
    let old_isk = &iskeyword
    setl iskeyword+=:
    let str = expand("<cword>")
    let &l:iskeyword = old_isk
    execute 'Man ' . str
endfunction
command! JbzCppMan :call s:JbzCppMan()
au FileType cpp nnoremap <buffer>K :JbzCppMan<CR>


"""""""""""""""""""""
" vimairline config "
"""""""""""""""""""""

colorscheme gruvbox
let g:airline_powerline_fonts = 1


""""""""""""""
" COC config "
""""""""""""""

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
if has("patch-8.1.1564")
    " Recently vim can merge signcolumn and number column into one
    set signcolumn=number
else
    set signcolumn=yes
endif

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
            \ pumvisible() ? "\<C-n>" :
            \ <SID>check_back_space() ? "\<TAB>" :
            \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
    let col = col('.') - 1
    return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
if has('nvim')
    inoremap <silent><expr> <c-space> coc#refresh()
else
    inoremap <silent><expr> <c-@> coc#refresh()
endif

" Make <CR> auto-select the first completion item and notify coc.nvim to
" format on enter, <cr> could be remapped by other vim plugin
inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm()
            \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

" Use `[g` and `]g` to navigate diagnostics
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
    if (index(['vim','help'], &filetype) >= 0)
        execute 'h '.expand('<cword>')
    elseif (coc#rpc#ready())
        call CocActionAsync('doHover')
    else
        execute '!' . &keywordprg . " " . expand('<cword>')
    endif
endfunction

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" Formatting selected code.
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
    autocmd!
    " Setup formatexpr specified filetype(s).
    autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
    " Update signature help on jump placeholder.
    autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current buffer.
nmap <leader>ac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

" Map function and class text objects
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
omap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap af <Plug>(coc-funcobj-a)
xmap ic <Plug>(coc-classobj-i)
omap ic <Plug>(coc-classobj-i)
xmap ac <Plug>(coc-classobj-a)
omap ac <Plug>(coc-classobj-a)

" Remap <C-f> and <C-b> for scroll float windows/popups.
" Note coc#float#scroll works on neovim >= 0.4.0 or vim >= 8.2.0750
nnoremap <nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
nnoremap <nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
inoremap <nowait><expr> <C-f> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<Right>"
inoremap <nowait><expr> <C-b> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<Left>"

" NeoVim-only mapping for visual mode scroll
" Useful on signatureHelp after jump placeholder of snippet expansion
if has('nvim')
    vnoremap <nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#nvim_scroll(1, 1) : "\<C-f>"
    vnoremap <nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#nvim_scroll(0, 1) : "\<C-b>"
endif

" Use CTRL-S for selections ranges.
" Requires 'textDocument/selectionRange' support of language server.
nmap <silent> <C-s> <Plug>(coc-range-select)
xmap <silent> <C-s> <Plug>(coc-range-select)

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocAction('format')

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Add (Neo)Vim's native statusline support.
" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline.
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Mappings for CoCList
" Show all diagnostics.
nnoremap <silent><nowait> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions.
nnoremap <silent><nowait> <space>e  :<C-u>CocList extensions<cr>
" Show commands.
nnoremap <silent><nowait> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document.
nnoremap <silent><nowait> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols.
nnoremap <silent><nowait> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent><nowait> <space>cj  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent><nowait> <space>ck  :<C-u>CocPrev<CR>
" Resume latest coc list.
nnoremap <silent><nowait> <space>p  :<C-u>CocListResume<CR>


""""""""""""""""""""""""
" Fugitive Git Pluggin "
""""""""""""""""""""""""

if empty($BEPO) "azerty
    nnoremap <leader>gs :vert Git<CR>
    nnoremap <leader>gb :GBranches<CR>
    nnoremap <leader>gt :Git pull<CR>
    nnoremap <leader>gp :Git push<CR>

    " keep "ours" in merge (left part)
    nnoremap <leader>gh :diffget //2<CR>
    " keep "theirs" in merge (right part)
    nnoremap <leader>gl :diffget //3<CR>
else                                              "bepo
    nnoremap <leader>,s :vert Git<CR>
    nnoremap <leader>,b :GBranches<CR>
    nnoremap <leader>,t :Git pull<CR>
    nnoremap <leader>,p :Git push<CR>
    nnoremap <leader>,c :diffget //2<CR>
    nnoremap <leader>,r :diffget //3<CR>

    let g:nremap = {'r': 'h'}
    let g:xremap = {'r': 'h'}
    let g:oremap = {'r': 'h'}
    let g:nremap = {'c': 'l'}
    let g:xremap = {'c': 'l'}
    let g:oremap = {'c': 'l'}
    let g:nremap = {'s': 'k'}
    let g:xremap = {'s': 'k'}
    let g:oremap = {'s': 'k'}
endif

""""""""""""""""""""""""""""
" Fuzzy Finder fzf Pluggin "
""""""""""""""""""""""""""""

if empty($BEPO) "azerty
    " search in git files
    nnoremap <leader>fk :GFiles<CR>
    " search in already opened files
    nnoremap <leader>fj :History<CR>
else                                              "bepo
    nnoremap <leader>fr :GFiles<CR>
    nnoremap <leader>fs :History<CR>
endif

" faire apparaitre le fenettre fuzzy au milieu de l'ecran
let g:fzf_layout = { 'window': { 'width': 0.8, 'height': 0.8 } }

""""""""""""""""""""""""""""""""""""""""""""""""""""
" Vista : code structure explorer with lsp support "
""""""""""""""""""""""""""""""""""""""""""""""""""""

nnoremap <silent> <A-v>s :Vista!!<CR>


""""""""""""""""""
" vim-autoformat "
""""""""""""""""""

" formate le code
noremap <leader>a :Autoformat<CR>

" formate le code à l'ecriture du fichier
" au BufWrite * :Autoformat


""""""""""""""
" vimspector "
""""""""""""""

let g:vimspector_enable_mappings = 'HUMAN'

fun! GotoWindow(id)
    call win_gotoid(a:id)
    MaximizerToggle
endfun

" Debugger remaps
nnoremap <F2> :VimspectorReset<CR>
nnoremap <leader>m :MaximizerToggle!<CR>

nnoremap <leader>dd :call vimspector#Launch()<CR>
nnoremap <leader>dc :call GotoWindow(g:vimspector_session_windows.code)<CR>
nnoremap <leader>dt :call GotoWindow(g:vimspector_session_windows.tagpage)<CR>
nnoremap <leader>dv :call GotoWindow(g:vimspector_session_windows.variables)<CR>
nnoremap <leader>dw :call GotoWindow(g:vimspector_session_windows.watches)<CR>
nnoremap <leader>ds :call GotoWindow(g:vimspector_session_windows.stack_trace)<CR>
nnoremap <leader>do :call GotoWindow(g:vimspector_session_windows.output)<CR>
nnoremap <leader>de :call vimspector#Reset()<CR>

"""""""""""""
" ultisnips "
"""""""""""""

" snippets editing buffer opens in horizontal split
let g:UltiSnipsEditSplit = 'horizontal'

let g:UltiSnipsExpandTrigger = '<tab>'
let g:UltiSnipsJumpForwardTrigger = '<tab>'
let g:UltiSnipsJumpBackwardTrigger = '<s-tab>'

" .pro (getDP) snippets priority : pro > onelab > all (discard any others)
autocmd BufNewFile,BufReadPost *.pro UltiSnipsAddFiletypes pro.onelab
autocmd BufNewFile,BufReadPost *.pro UltiSnipsAddFiletypes onelab.all

" .geo (gmsh)  snippets priority : geo > onelab > all (discard any others)
autocmd BufNewFile,BufReadPost *.geo UltiSnipsAddFiletypes geo.onelab
autocmd BufNewFile,BufReadPost *.geo UltiSnipsAddFiletypes onelab.all

""""""""""
" VimTex "
""""""""""

let g:vimtex_view_method = 'zathura'
let g:tex_flavor = 'latex'

" Compile on initialization, cleanup on quit
augroup vimtex_event_1
  au!
  au User VimtexEventInitPost call vimtex#compiler#compile()
  au User VimtexEventQuit     call vimtex#compiler#clean(0)
augroup END

" Leader pour les commandes en mode insert VimTex
if empty($BEPO) "azerty
    autocmd filetype tex,latex unmap ù
    let g:vimtex_imaps_leader = 'ù'
    " remap % en ùm
    autocmd filetype tex,latex nmap ùm <Plug>(vimtex-%)
else
    autocmd filetype tex,latex unmap ç
    let g:vimtex_imaps_leader = 'ç'
    " remap % en çm
    autocmd filetype tex,latex nmap çm <Plug>(vimtex-%)
endif

" Saut dans le document pdf
autocmd FileType tex,latex nnoremap <buffer> m :VimtexView<CR>
" Texte sur plusieurs lignes
autocmd FileType tex,latex set wrap

" Fermeture de la quickfix list de compilation après n deplacements
let g:vimtex_quickfix_autoclose_after_keystrokes = 4
" Ignore 'Overfull \hbox' error in log
let g:vimtex_quickfix_ignore_filters = [
      \ 'Overfull',
      \ 'undefined references',
      \ '(re)run Biber',
      \]

" Affichage des symbole math comme dans le document
set conceallevel=2
let g:tex_conceal='abdmg'

" Mappings mode insertion, pour macros electromag
call vimtex#imaps#add_map({
      \ 'lhs' : 'N',
      \ 'rhs' : '\nabla', })
call vimtex#imaps#add_map({
      \ 'lhs' : 'b',
      \ 'rhs' : '\b', })
call vimtex#imaps#add_map({
      \ 'lhs' : 'h',
      \ 'rhs' : '\H', })
call vimtex#imaps#add_map({
      \ 'lhs' : 'l',
      \ 'rhs' : '\L', })

" Racourcis pour ensemble
call vimtex#imaps#add_map({
      \ 'lhs' : 'oe',
      \ 'rhs' : '\omeSymb', })
call vimtex#imaps#add_map({
      \ 'lhs' : 'os',
      \ 'rhs' : '\omsSymb', })
call vimtex#imaps#add_map({
      \ 'lhs' : 'oc',
      \ 'rhs' : '\omcSymb', })
call vimtex#imaps#add_map({
      \ 'lhs' : 'ode',
      \ 'rhs' : '\domeSymb', })
call vimtex#imaps#add_map({
      \ 'lhs' : 'ods',
      \ 'rhs' : '\domsSymb', })
call vimtex#imaps#add_map({
      \ 'lhs' : 'odc',
      \ 'rhs' : '\domcSymb', })

if empty($BEPO) "azerty
    " Redefinition des mapping donnes en remplacant [ et ] par ( et )
    nmap )) <plug>(vimtex-]])
    nmap )( <plug>(vimtex-][)
    nmap () <plug>(vimtex-[])
    nmap (( <plug>(vimtex-[[)
    xmap )) <plug>(vimtex-]])
    xmap )( <plug>(vimtex-][)
    xmap () <plug>(vimtex-[])
    xmap (( <plug>(vimtex-[[)
    omap )) <plug>(vimtex-]])
    omap )( <plug>(vimtex-][)
    omap () <plug>(vimtex-[])
    omap (( <plug>(vimtex-[[)
    "
    nmap )M <plug>(vimtex-]M)
    nmap )m <plug>(vimtex-]m)
    nmap (M <plug>(vimtex-[M)
    nmap (m <plug>(vimtex-[m)
    xmap )M <plug>(vimtex-]M)
    xmap )m <plug>(vimtex-]m)
    xmap (M <plug>(vimtex-[M)
    xmap (m <plug>(vimtex-[m)
    omap )M <plug>(vimtex-]M)
    omap )m <plug>(vimtex-]m)
    omap (M <plug>(vimtex-[M)
    omap (m <plug>(vimtex-[m)
    "
    nmap )N <plug>(vimtex-]N)
    nmap )n <plug>(vimtex-]n)
    nmap (N <plug>(vimtex-[N)
    nmap (n <plug>(vimtex-[n)
    xmap )N <plug>(vimtex-]N)
    xmap )n <plug>(vimtex-]n)
    xmap (N <plug>(vimtex-[N)
    xmap (n <plug>(vimtex-[n)
    omap )N <plug>(vimtex-]N)
    omap )n <plug>(vimtex-]n)
    omap (N <plug>(vimtex-[N)
    omap (n <plug>(vimtex-[n)
    "
    nmap )R <plug>(vimtex-]R)
    nmap )r <plug>(vimtex-]r)
    nmap (R <plug>(vimtex-[R)
    nmap (r <plug>(vimtex-[r)
    xmap )R <plug>(vimtex-]R)
    xmap )r <plug>(vimtex-]r)
    xmap (R <plug>(vimtex-[R)
    xmap (r <plug>(vimtex-[r)
    omap )R <plug>(vimtex-]R)
    omap )r <plug>(vimtex-]r)
    omap (R <plug>(vimtex-[R)
    omap (r <plug>(vimtex-[r)
    "
    nmap )/ <plug>(vimtex-]/)
    nmap )* <plug>(vimtex-]*)
    nmap (/ <plug>(vimtex-[/)
    nmap (* <plug>(vimtex-[*)
    xmap )/ <plug>(vimtex-]/)
    xmap )* <plug>(vimtex-]*)
    xmap (/ <plug>(vimtex-[/)
    xmap (* <plug>(vimtex-[*)
    omap )/ <plug>(vimtex-]/)
    omap )* <plug>(vimtex-]*)
    omap (/ <plug>(vimtex-[/)
    omap (* <plug>(vimtex-[*)

    " Remap % en ù en normal mode
    "
    nmap <silent> ù  <Plug>(MatchitNormalForward)
    nmap <silent> gù <Plug>(MatchitNormalBackward)
    xmap <silent> ù  <Plug>(MatchitVisualForward)
    xmap <silent> gù <Plug>(MatchitVisualBackward)
    omap <silent> ù  <Plug>(MatchitOperationForward)
    omap <silent> gù <Plug>(MatchitOperationBackward)

    nmap <silent> [ù <Plug>(MatchitNormalMultiBackward)
    nmap <silent> ]ù <Plug>(MatchitNormalMultiForward)
    xmap <silent> [ù <Plug>(MatchitVisualMultiBackward)
    xmap <silent> ]ù <Plug>(MatchitVisualMultiForward)
    omap <silent> [ù <Plug>(MatchitOperationMultiBackward)
    omap <silent> ]ù <Plug>(MatchitOperationMultiForward)

    " text object:
    xmap aù <Plug>(MatchitVisualTextObject)
else
    " remap t,c en j,l en normal mode
    "
    let g:vimtex_mappings_disable = {
        \ 'n': ['tsd', 'tsd', 'tsf', 'tsc', 'tse', 'csd', 'csc', 'cse', 'cs$'],
        \ 'x': ['tsd', 'tsd', 'tsf'],
        \}
    nmap lse <plug>(vimtex-env-change)
    nmap lsc <plug>(vimtex-cmd-change)
    nmap ls$ <plug>(vimtex-env-change-math)
    nmap lsd <plug>(vimtex-delim-change-math)
    nmap jsf <plug>(vimtex-cmd-toggle-frac)
    xmap jsf <plug>(vimtex-cmd-toggle-frac)
    nmap jsc <plug>(vimtex-cmd-toggle-star)
    nmap jse <plug>(vimtex-env-toggle-star)
    nmap js$ <plug>(vimtex-env-toggle-math)
    nmap jsd <plug>(vimtex-delim-toggle-modifier)
    xmap jsd <plug>(vimtex-delim-toggle-modifier)
    nmap jsd <plug>(vimtex-delim-toggle-modifier-reverse)
    xmap jsd <plug>(vimtex-delim-toggle-modifier-reverse)

    " Remap % en ç en normal mode
    "
    nmap <silent> ç  <Plug>(MatchitNormalForward)
    nmap <silent> gç <Plug>(MatchitNormalBackward)
    xmap <silent> ç  <Plug>(MatchitVisualForward)
    xmap <silent> gç <Plug>(MatchitVisualBackward)
    omap <silent> ç  <Plug>(MatchitOperationForward)
    omap <silent> gç <Plug>(MatchitOperationBackward)

    nmap <silent> [ç <Plug>(MatchitNormalMultiBackward)
    nmap <silent> ]ç <Plug>(MatchitNormalMultiForward)
    xmap <silent> [ç <Plug>(MatchitVisualMultiBackward)
    xmap <silent> ]ç <Plug>(MatchitVisualMultiForward)
    omap <silent> [ç <Plug>(MatchitOperationMultiBackward)
    omap <silent> ]ç <Plug>(MatchitOperationMultiForward)

    " text object:
    xmap aç <Plug>(MatchitVisualTextObject)
endif
