sudo pacman -Syyu
sudo pacman -S paru
sudo pacman -S yay
sudo pacman -S base-devel
yay paru
paru -S neovim-git

git clone git@gricad-gitlab.univ-grenoble-alpes.fr:marteaua/vim-configuration.git ~/dotfiles
#ou git clone https://gricad-gitlab.univ-grenoble-alpes.fr/marteaua/vim-configuration.git ~/dotfiles

ln -sf ~/dotfiles/config/nvim/init.vim ~/init.vim
ln -sf ~/dotfiles/.zshrc ~/.zshrc
ln -sf ~/dotfiles/config/paru ~/.config/paru
ln -sf ~/dotfiles/config/picom ~/.config/picom
ln -sf ~/dotfiles/config/nvim ~/.config/nvim
ln -sf ~/dotfiles/config/kitty ~/.config/kitty
ln -sf ~/dotfiles/config/i3 ~/.config/i3
ln -sf ~/dotfiles/config/i3status ~/.config/i3status

# Recuperer clefs ssh
ls .ssh
chmod 600 .ssh/rsa_antoine
chmod 644 .ssh/rsa_antoine.pub
ssh-add ~/.ssh/rsa_antoine

sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

ssh-add -L
paru signal-desktop
sudo pacman -S wireguard-tools\

# super ligne paru
paru gdb cmake ccls kitty

# detail paru
paru i3-gaps
paru i3lock
paru i3status
paru zathura
sudo pacman -S traceroute
paru nodejs
paru flameshot
paru xorg-xprop
paru -S nfs-utils
paru libreoffice-fresh-fr
paru libreoffice-fresh-en-gb
paru udiskie
paru redshift
paru zotero
paru pa-applet-git
paru cppman
paru clang-format
paru universal-ctags
paru nm_applet
paru kitty
paru ccls
paru cmake
paru gdb
paru nitrogen
paru picom
paru rofi
paru xdotool
sudo pacman -S texlive-most

paru python-pip
pip install -U pip
pip install neovim-remote
pip install pynvim
pip install numpy
pip install matplotlib

paru npm
sudo npm install -g neovim

# vim-plug
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
# ouvrir vim, faire :PlugInstall et :checkhealth

# theme interface graphique
sudo pacman -S lxappearance
lxappearance

mkdir prog
cd prog
git clone https://gitlab.onelab.info/doc/sandbox.git sandbox
git clone git@gricad-gitlab.univ-grenoble-alpes.fr:marteaua/these_antoine.git these

#setup acces distance
paru xorgxrdp
sudo systemctl enable xrdp
sudo systemctl start xrdp
systemctl list-units
sudo journalctl -u xrdp
# dans /etc/xrdp/xrdp.ini ajouter tout les protocoles TLS
# verifier dans /etc/xrdp/startwm.sh que i3 est demandé, et dans ~/.xinitrc que ca lance bien i3
